'use strict';

importScripts('https://storage.googleapis.com/workbox-cdn/releases/4.3.1/workbox-sw.js');

if (workbox) {
    workbox.routing.registerRoute(
        /\.js$/,
        new workbox.strategies.CacheFirst({cacheName:'js-cache'})
    );
    workbox.routing.registerRoute(
        /\.css$/,
        new workbox.strategies.CacheFirst({
            cacheName: 'css-cache',
        })
    );
    
    workbox.routing.registerRoute(
        /\.(?:png|jpg|jpeg|svg|gif)$/,
        new workbox.strategies.CacheFirst({
            cacheName: 'image-cache',
            plugins: [
                new workbox.expiration.Plugin({
                    maxEntries: 20,
                    maxAgeSeconds: 7 * 24 * 60 * 60,
                })
            ],
        })
    );
    workbox.routing.registerRoute(
        '/',
        new workbox.strategies.CacheFirst({
            cacheName: 'other-cache',
        })
    );
}
self.addEventListener('install', (evt) => {
    console.log('[ServiceWorker] Install');
    self.skipWaiting();
});

self.addEventListener('activate', (evt) => {
    console.log('[ServiceWorker] Activate');
    self.clients.claim();
});

self.addEventListener('fetch', (evt) => {
    console.log('[ServiceWorker] Fetch', evt.request.url);
});

